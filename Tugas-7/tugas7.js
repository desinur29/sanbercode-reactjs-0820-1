//Soal 1
console.log("-- Soal 1 --");

class Animal{
    constructor(name){
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name(){
       return this._name;
    }

    set name(x){
        this._name = x;
    }

    get legs(){
        return this._legs;
    }

    set legs(x){
        this._legs = x;
    }

    get cold_blooded(){
        return this._cold_blooded;
    }

    set cold_blooded(x){
        this.cold_blooded = x;
    }

}
//Release 0
console.log("-- Release 0 --");

let sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log("");

//Release 1
console.log("-- Release 1 --");

//Class Ape
class Ape extends Animal{
    constructor(name){
        super(name);
        this._legs = 2;
    }

    yell(){
        return console.log("Auooooo")
    }
}

//Class Frog
class Frog extends Animal{
    constructor(name){
        super(name);
    }

    jump(){
        return console.log("hop hop");
    }
}

let sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"
 
let kodok = new Frog("buduk");
kodok.jump() // "hop hop" 

console.log();


//Soal 2
console.log("-- Soal 2 --");

class Clock{
    constructor({template : x}){
        this.template = x;
        this.times = null;
    }

    render(){

        let date = new Date();

        let hours = date.getHours();
        if(hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if(mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if(secs < 10) secs = '0' + secs;

        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop(){
        clearInterval(this.times);
    }

    start(){
        this.render();
        this.times = setInterval(this.render.bind(this), 1000);
    }
}
 

let clock = new Clock({template: 'h:m:s'});
clock.start(); 