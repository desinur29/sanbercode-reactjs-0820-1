//Soal 1
console.log("-- Soal 1 --");

const pi = 3.14;
let r;

const luasLingkaran = (r)=>{return pi*r*r };
const kelilingLingkaran = (r)=>{return 2*pi*r};

console.log("Luas Lingkaran : " +luasLingkaran(6));
console.log("Keliling Lingkaran: "+kelilingLingkaran(5));

//Soal 2
console.log("-- Soal 2 --");

let kalimat ="";

const gabungKata = (kata1, kata2, kata3, kata4, kata5) => {
    kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`;
    console.log(kalimat);
}

gabungKata("saya", "adalah","seorang","frontend","developer");

//Soal 3
console.log("-- Soal 3 --");

const newFunction = (firstName, lastName) => {
    return{
      firstName,
      lastName,
      fullName(){
        console.log(`${firstName} ${lastName}`);
        return;
      }
    }
  }
   
  newFunction("William", "Imoh").fullName() 


//Soal 4
console.log("-- Soal 4 --")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation);

//Soal 5
console.log("-- Soal 5 --")

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west,...east];
console.log(combined);




