/*
for([Inisialisasi], [Kondisi], [Incremental/Decremental]) {
  [Proses] // Merupakan proses yang akan dijalankan dalam satu iterasi
} 
*/

//For Loop 1
for(var angka = 1; angka < 10; angka++){
    console.log('Iterasi ke-' +angka);
}

//For Loop 2
var jumlah = 0;
for(var deret = 5; deret > 0; deret --){
    jumlah += deret;
    console.log('Jumlah saat ini : '+jumlah)
}

console.log('Jumlah terakhir : '+jumlah);

//For Loop 3
for(var deret = 0; deret < 10; deret += 2) {
    console.log('Iterasi dengan Increment counter 2: ' + deret);
  }
   
  console.log('-------------------------------');
   
  for(var deret = 15; deret > 0; deret -= 3) {
    console.log('Iterasi dengan Decrement counter : ' + deret);
  } 