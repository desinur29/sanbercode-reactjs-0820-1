//Soal 1
var angka = 0;
console.log('LOOPING PERTAMA');
while(angka < 20){
    angka +=2;
    console.log( angka+ " - I love coding");
}
console.log('LOOPING KEDUA');
while(angka  > 0){
    console.log( angka+ " - I will become a frontend developer");
    angka -= 2;
}

//Soal 2
console.log('Soal no 2');
for(var angka = 1; angka <= 20 ; angka++){
    if (angka % 3 === 0 && angka % 2 === 1){
        console.log( angka+ ' - I love Coding');
    }else if (angka % 2 === 1){
        console.log( angka+ ' - Santai');
    }else if (angka % 2 === 0){
        console.log(angka+ ' - Berkualitas')
    }else{
        console.log( angka + 'angka tidak masuk kategori')
    }
}

//Soal 3
console.log('Soal 3');

for( var angka = 0; angka < 7; angka++){
    var a = '';
   for( angka2 = 0; angka2 <= angka; angka2++){
       a += '#';
   }
   console.log(a);
}

//Soal 4
console.log('Soal 4');
var kalimat = "saya sangat senang belajar javascript";
var a = kalimat.split(' ');
console.log(a);


//Soal 5
console.log('Soal 5');
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
var angka = 0;
while(angka <= 4){
    console.log(daftarBuah[angka]);
    angka++;
}

