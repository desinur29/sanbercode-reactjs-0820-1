// Looping While loop 1
var flag = 1;
while (flag <= 10) {
    console.log('Iterasi ke-' + flag);
    flag++;
}

//Looping While Loop 2
var deret = 5;
var jumlah = 0;
while (deret > 0){
    jumlah += deret; //jumlah = jumlah + deret
    deret--; // deret = deret - 1
    console.log("Jumlah saati ini : " +jumlah)
}

console.log(jumlah);