//Soal 1

console.log('--- Soal 1 ---');

function halo(){
    return '"Halo Sanbers!"';
}

console.log(halo());

//Soal 2

console.log('--- Soal 2 ---');

function kalikan(num1, num2){
    return num1 * num2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);


//Soal 3

console.log('--- Soal 3 ---');

function introduce(name, age, address, hobby){
    return '"Nama Saya ' + name +', umur saya '+ age + ' tahun, alamat saya di ' 
    + address + ' dan saya punya hobby yaitu ' + hobby + '!"'
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

//Soal 4

console.log('--- Soal 4 ---');

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];

var daftarPeseta = {
    nama : arrayDaftarPeserta[0],
    jeniskelamin : arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    tahunlahir : arrayDaftarPeserta[3]
}

console.log(daftarPeseta);


//Soal 5

console.log('--- Soal 5 ---');

var dataBuah =[
    {
        nama: 'strawberry',
        warna: 'merah',
        adaBijinya : false,
        harga: 9000 
    },
    {
        nama: 'jeruk',
        warna: 'oranye',
        adaBijinya: true,
        harga: 8000
    },
    {
        nama: 'Semangka',
        warna: 'Hijau & Merah',
        adaBijinya : true,
        harga: 10000

    },
    {
        nama: 'Pisang',
        warna: 'Kuning',
        adaBijinya: false,
        harga: 5000

    }
];

console.log(dataBuah[0]);


//Soal 6

console.log('--- Soal 6 ---');

var dataFilm = [];
    
function tambahData(data1, data2, data3, data4){
 dataFilm.push({
    nama : data1,
    durasi : data2,
    genre : data3,
    tahun : data4
});
}

tambahData("AADC", "120 Menit", "Romantis", "2000");
tambahData("PENISULA", "140 Menit", "Thriller", "2020");
tambahData("Black Butler", "100 Menit", "Action", "2015");
console.log(dataFilm);







